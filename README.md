# league-serverless

Part of League of Legends statistics site, built for a tech screening.

## Deployments
- ~~[Live React site] (https://keen-knuth-c1a4df.netlify.com/)~~  Search disabled, Riot API dev key expired, no plans to regenerate at this time.

## The Backend
[Backend source code](https://gitlab.com/danhewlett/league-serverless)
- Node.js
- AWS Lambda
- API Gateway
- CloudWatch
- Deployed via Serverless Framework (infrastructure as code)
- Custom module for Riot API v4

## The Frontend
[React site source code](https://gitlab.com/danhewlett/league-site)
- React.js
- Material-UI
- Continous deployment via Netlify GitLab hooks