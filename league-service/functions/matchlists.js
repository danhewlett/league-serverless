'use strict';


module.exports.handler = async function(event, context) {

    var rito_api = require('rito_api');

    const summoner_name = event.pathParameters.summoner_name;
    const summoner = await rito_api.summoners_by_name(summoner_name);
    const matchlist = await rito_api.matchlists_by_account_id(summoner.accountId);

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            summoner: summoner,
            matchlist: matchlist
        }),
    };
    // Use this code if you don't use the http event with the LAMBDA-PROXY integration
    // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};