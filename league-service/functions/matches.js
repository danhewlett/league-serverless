'use strict';


module.exports.handler = async function(event, context) {

    var rito_api = require('rito_api');

    const gameId = event.pathParameters.gameId;
    const match = await rito_api.matches_by_match_id(gameId);

    return {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            match: match
        }),
    };
    // Use this code if you don't use the http event with the LAMBDA-PROXY integration
    // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};