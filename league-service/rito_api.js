var secrets = require('secrets');
var api = {}

api.key = secrets.rito_api_key;

api.summoners_by_name = async function (summoner_name) {
    let fetch = require('node-fetch');
    let url = `https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/${summoner_name}?api_key=${api.key}`;
    const response = await fetch(url);
    const json = await response.json();
    return json;
}

api.matches_by_match_id = async function (match_id) {
    let fetch = require('node-fetch');
    let url = `https://na1.api.riotgames.com/lol/match/v4/matches/${match_id}?api_key=${api.key}`;
    const response = await fetch(url);
    const json = await response.json();
    return json;
}

api.matchlists_by_account_id = async function (account_id) {
    let fetch = require('node-fetch');
    let url = `https://na1.api.riotgames.com/lol/match/v4/matchlists/by-account/${account_id}?endIndex=5&beginIndex=0&api_key=${api.key}`;
    const response = await fetch(url);
    const json = await response.json();
    return json;
}

module.exports = api;